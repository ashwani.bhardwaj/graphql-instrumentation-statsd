package com.test.graphql.resolver;

import com.test.graphql.model.Author;
import com.test.graphql.model.Book;
import com.test.graphql.persistence.AuthorRepository;
import com.test.graphql.persistence.BookRepository;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Query implements GraphQLQueryResolver {

  private BookRepository bookRepository;

  private AuthorRepository authorRepository;

  public Iterable<Book> findAllBooks() {
    return bookRepository.findAll();
  }

  public Iterable<Author> findAllAuthors() {
    return authorRepository.findAll();
  }

  public int countBooks() {
    return (int) bookRepository.count();
  }

  public int countAuthors() {
    return (int) authorRepository.count();
  }

}
