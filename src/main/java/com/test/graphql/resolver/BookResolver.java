package com.test.graphql.resolver;

import com.test.graphql.model.Author;
import com.test.graphql.model.Book;
import com.test.graphql.persistence.AuthorRepository;
import graphql.kickstart.tools.GraphQLResolver;

import java.util.Optional;

public class BookResolver implements GraphQLResolver<Book> {

  private AuthorRepository authorRepository;

  public BookResolver(AuthorRepository authorRepository) {
    this.authorRepository = authorRepository;
  }

  public Optional<Author> getAuthor(Book book) {
    return authorRepository.findById(book.getAuthor().getId());
  }

}
