package com.test.graphql.resolver;

import com.test.graphql.exception.BookNotFoundException;
import com.test.graphql.model.Author;
import com.test.graphql.model.Book;
import com.test.graphql.persistence.AuthorRepository;
import com.test.graphql.persistence.BookRepository;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Mutation implements GraphQLMutationResolver {

  private BookRepository bookRepository;

  private AuthorRepository authorRepository;

  public Author newAuthor(String firstName, String lastName) {
    Author author = Author.builder()
        .firstName(firstName)
        .lastName(lastName)
        .build();

    return authorRepository.save(author);
  }

  public Book newBook(String title, String isbn, Integer pageCount, Long authorId) {

    Book book = Book.builder()
        .title(title)
        .isbn(isbn)
        .pageCount(pageCount != null ? pageCount : 0)
        .author(new Author(authorId))
        .build();

    return bookRepository.save(book);
  }

  public boolean deleteBook(Long id) {
    bookRepository.deleteById(id);
    return true;
  }

  public Book updateBookPageCount(Integer pageCount, Long id) {
    Book book = bookRepository.findById(id)
        .orElseThrow(() -> new BookNotFoundException("The book to be updated was not found", id));

    book.setPageCount(pageCount);
    return bookRepository.save(book);
  }

}
