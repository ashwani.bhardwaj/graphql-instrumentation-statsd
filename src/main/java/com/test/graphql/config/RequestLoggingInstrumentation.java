package com.test.graphql.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.ExecutionResult;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.SimpleInstrumentationContext;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import graphql.execution.instrumentation.parameters.InstrumentationFieldFetchParameters;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RequestLoggingInstrumentation extends SimpleInstrumentation {

    private static final Logger logger = LoggerFactory.getLogger(RequestLoggingInstrumentation.class);

    @Override
    public InstrumentationContext<ExecutionResult> beginExecution(InstrumentationExecutionParameters parameters) {
        long startMillis = System.currentTimeMillis();
        var executionId = parameters.getExecutionInput().getExecutionId();
        // logger.info("Operation Name {}", findDatafetcherTag(parameters));

        if (logger.isInfoEnabled()) {
            logger.info("RequestLoggingInstrumentation: GraphQL execution {} started", executionId);

            var query = parameters.getQuery();
            logger.info("RequestLoggingInstrumentation: [{}] query: {}", executionId, query);
            if (parameters.getVariables() != null && !parameters.getVariables().isEmpty()) {
                logger.info("RequestLoggingInstrumentation: [{}] variables: {}", executionId, parameters.getVariables());
            }
        }

        return new SimpleInstrumentationContext<>() {
            @Override
            public void onCompleted(ExecutionResult executionResult, Throwable t) {
                if (logger.isInfoEnabled()) {
                    long endMillis = System.currentTimeMillis();

                    if (t != null) {
                        logger.info("RequestLoggingInstrumentation: GraphQL execution {} failed: {}", executionId, t.getMessage(), t);
                    } else {
                        var resultMap = executionResult.toSpecification();
                        String resultJSON = null;
                        try {
                            resultJSON = new ObjectMapper().writeValueAsString(resultMap).replace("\n", "\\n");
                            logger.info("RequestLoggingInstrumentation: [{}] completed in {}ms", executionId, endMillis - startMillis);
                            //logger.info("[{}] result: {}", executionId, resultJSON);
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }

    private String findDatafetcherTag(InstrumentationFieldFetchParameters parameters) {
        GraphQLOutputType type = parameters.getExecutionStepInfo().getParent().getType();
        GraphQLObjectType parent;
        if (type instanceof GraphQLNonNull) {
            parent = (GraphQLObjectType) ((GraphQLNonNull) type).getWrappedType();
        } else {
            parent = (GraphQLObjectType) type;
        }

        return  parent.getName() + "." + parameters.getExecutionStepInfo().getPath().getSegmentName();
    }
}
