package com.test.graphql;

import com.test.graphql.persistence.AuthorRepository;
import com.test.graphql.persistence.BookRepository;
import com.test.graphql.resolver.BookResolver;
import com.test.graphql.resolver.Mutation;
import com.test.graphql.resolver.Query;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GraphqlApplication {

	@Autowired
	AuthorRepository ar;

	@Autowired
	BookRepository br;

	public static void main(String[] args) {
		SpringApplication.run(GraphqlApplication.class, args);
	}

//	@Bean
//	public ServletRegistrationBean servletRegistrationBean() {
//
//		GraphQLSchema schema  = SchemaParser.newParser()
//				.resolvers(authorResolver(ar), mutation(br, ar), query(br, ar))
//				.file("graphql/author.graphqls")
//				.file("graphql/book.graphqls")
//				.build().makeExecutableSchema();
//		ExecutionStrategy executionStrategy = new AsyncExecutionStrategy();
//		GraphQLServlet servlet = new SimpleGraphQLServlet(schema, executionStrategy);
//		ServletRegistrationBean bean = new ServletRegistrationBean(servlet, "/graphql");
//		return bean;
//	}

//	@Bean
//	public GraphQLErrorHandler errorHandler() {
//		return new GraphQLErrorHandler() {
//			@Override
//			public List<GraphQLError> processErrors(List<GraphQLError> errors) {
//				List<GraphQLError> clientErrors = errors.stream()
//						.filter(this::isClientError)
//						.collect(Collectors.toList());
//
//				List<GraphQLError> serverErrors = errors.stream()
//						.filter(e -> !isClientError(e))
//						.map(GraphQLErrorAdapter::new)
//						.collect(Collectors.toList());
//
//				List<GraphQLError> e = new ArrayList<>();
//				e.addAll(clientErrors);
//				e.addAll(serverErrors);
//				return e;
//			}
//
//			protected boolean isClientError(GraphQLError error) {
//				return !(error instanceof ExceptionWhileDataFetching || error instanceof Throwable);
//			}
//		};
//	}

	@Bean
	public BookResolver authorResolver(AuthorRepository authorRepository) {
		return new BookResolver(authorRepository);
	}

	@Bean
	public Query query(BookRepository bookRepository, AuthorRepository authorRepository) {
		return new Query(bookRepository, authorRepository);
	}

	@Bean
	public Mutation mutation(BookRepository bookRepository, AuthorRepository authorRepository) {
		return new Mutation(bookRepository, authorRepository);
	}

//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurerAdapter() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("*").allowedOrigins("http://localhost:*");
//			}
//		};
//	}

	@Bean
	public MeterRegistryCustomizer<MeterRegistry> customize() {
		return registry -> {
			getConfig(registry);
			getDistributionSummary(registry);
		};
	}

	private void getDistributionSummary(MeterRegistry registry) {
		registry.config().meterFilter(new MeterFilter() {
			@Override
			public DistributionStatisticConfig configure(Meter.Id id, DistributionStatisticConfig config) {
				if(Meter.Type.TIMER.equals(id.getType()) && id.getName().matches("^(http|hystrix){1}.*")) {
					return DistributionStatisticConfig.builder().percentilesHistogram(true)
							.percentiles(0.1,0.25,0.5,0.9,0.95).build().merge(config);
				}
				return config;
			}
		});
	}

	private void getConfig(MeterRegistry registry) {
		registry.config().commonTags("application.name", "graphqlV2",
				"environment.name", "mydev");
	}


//	@Bean
//	GraphQLSchema schema() {
//		DataFetcher<String> test = env -> "response";
//		return GraphQLSchema.newSchema()
//				.query(GraphQLObjectType.newObject()
//						.name("query")
//						.field(field -> field
//								.name("test")
//								.type(Scalars.GraphQLString)
//						)
//						.build())
//				.codeRegistry(GraphQLCodeRegistry.newCodeRegistry()
//						.dataFetcher(FieldCoordinates.coordinates("query", "test"), test)
//						.build())
//				.build();
//	}

}
