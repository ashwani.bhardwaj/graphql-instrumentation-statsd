# GraphQL Instrumentation using micrometer statsD

Find details about this project [here](https://www.pluralsight.com/guides/building-a-graphql-server-with-spring-boot).

## Information
### 1. Project Info
This sample project is using spring-boot micrometer with graphql to demo exposure of various attributes for Instrumentation., using a simple Model of an Author and a Book.
An Author is an independent entity and a Book entity has @ManyToOne relation with Author.

Caution:  !!The GraphQL is backed by In-Memory H2 DB. A restart will wipe all saved data!!

### 2. Project Stack
Spring-boot, micrometer statsd, graphql(v 11.0.0), graphiql(v 11.0.0)

### 3. Run project and access graphiql
Jump to project directory and run ``` mvn clean spring-boot:run ```

Navigate to ``` http://localhost:8080/graphiql ```

### 4. GraphQL Queries
#### 4.1. Add an Author
```
mutation {
    newAuthor(firstName: "John", lastName: "Doe") {
        id
        firstName
        lastName
    }
 }
 ```
 #### 4.2. Add a Book with an Author info (Given author created in above step return Id: 1)
 ```
 mutation {
    newBook(title: "my title", isbn: "isbn", pageCount: 30, author: 1) {
        id
        title
    }
 }
 ```
 #### 4.3. Find all Books (with Author information)
 ```
 query getMeAllBooks {
  findAllBooks {
    id
    title
    isbn
    author {
      id
      firstName
      lastName
    }
  }
}
 ```
### 5. Checking attributes/tags for Instrumentation
The application will send data to data-dog agent running locally over UDP. In case of data-dog agent isn't available, no errors will be seen as it is over UDP.
However, one can check few attributes using actuator metrics end-point (enabled in application)
Jump to ``` http://localhost:8080/actuator/metrics ``` to look for metrics starting with "graphql"

### 6. Custom Instrumentation details
By default, few information is provided out of the box as mentioned here ``` https://github.com/graphql-java-kickstart/graphql-spring-boot#tracing-and-metrics ```

If one wishes to add custom instrumentation data, the following 3 classes are provided
#### 6.1. CustomMetricsInstrumentation
This enables to provide these two metrics:

``` graphql.counter.query.success ```

``` graphql.counter.query.error ```

#### 6.2. ExampleTracingInstrumentation and RequestLoggingInstrumentation
These both classes will provide total time taken by the query along with details on its various parameters like query name, async query result time taken, sub-query information.

### 7. How to write a new Instrumentation class 
Your custom class should ``` extends TracingInstrumentation``` or ``` extends SimpleInstrumentation``` and it will be picked up automatically by the library.

Refer this for [more](https://github.com/graphql-java-kickstart/graphql-spring-boot/blob/master/graphql-spring-boot-autoconfigure/src/main/java/graphql/kickstart/spring/web/boot/metrics/MetricsInstrumentation.java)

### 8. Online Resources
Refer to these pages for more information

- https://github.com/graphql-java-kickstart/graphql-spring-boot
- https://github.com/apollographql/apollo-tracing
- https://netflix.github.io/dgs/advanced/instrumentation/ 
- https://github.com/Netflix/dgs-examples-java/blob/main/src/main/java/com/example/demo/instrumentation/ExampleTracingInstrumentation.java